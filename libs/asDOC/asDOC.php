<?php
class asDOC {

	private $content;
	private $title;

	function __construct( $params=array() ) {
		$this->content = '';
		$this->title = isset($params['title']) ? $params['title'] : 'document';
	}

	public function writeHTML($html) {
		$html = preg_replace('/<pagebreak \/>/', '<div  style="page-break-before:always">&nbsp;</div>',  $html);
		$this->content .= $html;
		//pie($html);
		return true;
	}

	public function output() {

		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=".$this->title.".doc");

		echo $this->content;

		exit;

	}

}
