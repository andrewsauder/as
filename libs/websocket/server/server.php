<?php

//tricks for AS
	$_SERVER['SERVER_NAME'] = $argv[1];
	$_SERVER['SERVER_PORT'] = 0;
	$_SERVER['DOCUMENT_ROOT'] = __DIR__ . '/../../../../www';

//load AS framework
	include_once(__DIR__ . '/../../../bin/loader.php');

	$AS = new ASloader();
	$AS->startApp();

//load websocket library
	include_once('lib/loader.websocket.php');

//get server info
	$wsServerInfo = $_SESSION[AS_APP]['environment']['websocket'];

//create server
	$server = new Server($wsServerInfo['server'], $wsServerInfo['port'], strToBool($wsServerInfo['ssl']));

//server settings
	if(isset($wsServerInfo['allowed_origin'])) {
		$server->setCheckOrigin(true);
		$server->setAllowedOrigin($wsServerInfo['allowed_origin']);
	}
	else {
		$server->setCheckOrigin(false);
	}
	$server->setMaxClients($wsServerInfo['max_clients']);
	$server->setMaxConnectionsPerIp($wsServerInfo['max_connections_per_ip']);
	$server->setMaxRequestsPerMinute($wsServerInfo['max_requests_per_minute']);

//register apps
	$apps = ls(AS_VAR_PATH.'/websocket_apps/', true );

	foreach($apps as $app) {
		include_once(AS_VAR_PATH.'/websocket_apps/'.$app.'/loader.'.$app.'.php');
		$appClass = $app.'Application';
		$server->registerApplication($app, $appClass::getInstance());
	}

//run the server
	$server->run();