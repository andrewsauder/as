<?php

//facebook
require_once("core/libs/facebook/src/facebook.php");

//twitter
require 'core/libs/twitter/tmhOAuth.php';
require 'core/libs/twitter/tmhUtilities.php';


class social_media {
	
	//-----------------------------------------------------------------------------------------------------------------------------------
	//-- CONFIG -------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------
		
		//facebook
			private $facebook;
			private $facebook_config = array(
				'appId'  => '369458316457002',
				'secret' => 'a2a2d46747e487c0d0514301938d80b1'
			);
			private $facebook_page_id = '229366590460566';
			//if Andrew Sauder's FB account or employment is terminated, you will have to run $this->fb_getLifelongUserAccessToken 
			//and log in as another FB user who has admin permissions to the GC FB Page
			private $user_access_token = 'AAAFQBTe2NCoBAIs7Oq7kYjksikItRQocE1rZCo7XB6oOZAtIkSRNISRTRvxlanYL4u5ZBlDPVk1PUBFuGWmdEFvm56Bd74ZD';
			
			//the page access token is derived from the User account authenticated and with permissions to the GC Page
			private $page_access_token;
		
		//twitter
			private $twitter;
			private $twitter_config = array(
				  'consumer_key'    => 'ExHnq1sfC4CC4nOZ16Oeg',
				  'consumer_secret' => '0fzQJSiiPFvHyxbe8pGS4ezF9hxHp6fiEJSFL6csg',
				  'user_token'      => '400770846-mMaRCQ4S7gRr1LQTEuY5f9mKvkMUPqgFjZdseyS5',
				  'user_secret'     => 'Z05b4nen8grfrQ9cmYOBm8Zb2aQqfGTNHhv5l7gW9GI',
			);
			
		//all
			private $redirect_url;
			
	//-----------------------------------------------------------------------------------------------------------------------------------
	//-- END CONFIG ---------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------
	
	
	
	function __construct() {
		
		$this->redirect_url = $_SESSION['settings']['site_url'];
		
		//Facebook API - app created via Andrew Sauder - andrewsauder@gmail.com/dp7cmpd6
			$this->facebook = new Facebook($facebook_config);
		
		//Twitter API - app created via GCCommissioners/4ub2k96s
			$this->twitter = new tmhOAuth($twitter_config);
		
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------------------------
	//-- PUBLIC -------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------

		public function post( $message, $networks = array('fb','twitter') ) {
			
			//Facebook
			if(in_array('fb',$networks) || in_array('facebook',$networks)) {
				
				$step1 = $this->fb_getAccounts();
				$step2 = $this->facebook->api(
							'/'.$this->facebook_page_id.'/feed',
							'POST', 
							array(
								'access_token' => $this->page_access_token,
								'message' => $message
							)
						);
						
			}
			
			//Twitter
			if( in_array('twitter',$networks) ) {
				
				$code = $this->twitter->request('POST', $this->twitter->url('1/statuses/update'), array(
				  'status' => $message
				));
				
				if ($code == 200) {
					//tmhUtilities::pr(json_decode($this->twitter->response['response']));
				} else {
					//tmhUtilities::pr($this->twitter->response['response']);
				}
			
						
			}
				
		}



	//-----------------------------------------------------------------------------------------------------------------------------------
	//-- FACEBOOK CUSTOM ----------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------
	
		private function fb_getLifelongUserAccessToken() {
			
			//if the access code on line 12 dies, you must do this and then store the response in the line 12 variable.
			//this is how we automatically authenticate as Andrew Sauder so that we can post on the GC FB page
			
			if(!isset($_GET['code'])) {
				header('Location:https://www.facebook.com/dialog/oauth?client_id='. $this->facebook_config['appId'] .'&redirect_uri='. $this->redirect_url .'&scope=manage_pages,publish_stream,offline_access&state=gcgov'. time() );
			}
			
			$access_token_rsp = $facebook->api('/oauth/access_token','GET', array(
									'client_id'=>$this->facebook_config['appId'],
									'client_secret'=>$this->facebook_config['secret'],
									'redirect_uri'=>$this->redirect_url,
									'code'=>$_GET['code']
							)	);
			
			$access_token = explode('&',$access_token_rsp);
			$access_token = explode('=',$access_token[0]);
			
			$access_token = $access_token[1];
			
			echo 'Store this in the access_token variable defined with the class - around line 12: ';
			
			die($access_token);
			
		}
		
		private function fb_getAccounts() {
			//PULL DATA
			$readfeed = $facebook->api('/me/accounts','GET', array(
								'access_token' => $this->user_access_token
						)	);
			
			foreach($readfeed['data'] as $r) {
				if($r['id'] == '229366590460566') {
					$this->page_access_token = $r['access_token'];	
				}
			}
		}
	
		
}
?>