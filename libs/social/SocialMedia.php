<?php

//facebook
	require_once(AS__PATH.'libs/social/facebook/src/facebook.php');

//twitter
	require_once(AS__PATH.'libs/social/twitter/tmhOAuth.php');
	require_once(AS__PATH.'libs/social/twitter/tmhUtilities.php');


class SocialMedia {

	//-----------------------------------------------------------------------------------------------------------------------------------
	//-- CONFIG -------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------

		//control - change to true when in production
			private $enablePosting = false;

		//facebook
			private $facebook;

			private $facebook_config = array();

			private $facebook_page_id = '';

			private $user_access_token;

			//the page access token is derived from the User account authenticated and with permissions to the GC Page
			private $page_access_token;

		//twitter
			private $twitter;

			private $twitter_config = array();

		//all
			private $redirect_url;

	//-----------------------------------------------------------------------------------------------------------------------------------
	//-- END CONFIG ---------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------



	function __construct() {

		$this->redirect_url = $_SESSION[AS_APP]['settings']['site_url'];
		$this->user_access_token = $_SESSION[AS_APP]['settings']['fb_user_token'];

		$this->facebook_config['appId'] = $_SESSION['AS']['config']['social']['facebook']['appid'];
		$this->facebook_config['secret'] = $_SESSION['AS']['config']['social']['facebook']['secret'];
		$this->facebook_page_id = $_SESSION['AS']['config']['social']['facebook']['page_id'];

		$this->twitter_config['consumer_key'] = $_SESSION['AS']['config']['social']['twitter']['consumer_key'];
		$this->twitter_config['consumer_secret'] = $_SESSION['AS']['config']['social']['twitter']['consumer_secret'];
		$this->twitter_config['user_token'] = $_SESSION['AS']['config']['social']['twitter']['user_token'];
		$this->twitter_config['user_secret'] = $_SESSION['AS']['config']['social']['twitter']['user_secret'];

		if(!$_SESSION['testing']) {
			$this->enablePosting = true;
		}

		//Facebook API - app created via Andrew Sauder - andrewsauder@gmail.com/dp7cmpd6
			$this->facebook = new Facebook($this->facebook_config);
		//Twitter API - app created via GCCommissioners/4ub2k96s
			$this->twitter = new tmhOAuth($this->twitter_config);

	}



	//-----------------------------------------------------------------------------------------------------------------------------------
	//-- PUBLIC -------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------

		public function post( $message, $link='', $networks = array('fb','twitter') ) {

			//Facebook
			if(in_array('fb',$networks) && $this->enablePosting) {

				$step1 = $this->fb_getAccounts();
				$step2 = $this->facebook->api(
							'/'.$this->facebook_page_id.'/feed',
							'POST',
							array(
								'access_token' => $this->page_access_token,
								'message' => $message,
								'link' => $link
							)
						);

			}

			//Twitter
			if( in_array('twitter',$networks) && $this->enablePosting ) {

				$code = $this->twitter->request('POST', $this->twitter->url('1/statuses/update'), array(
				  'status' => substr($message,0,115).'...'
				));

				if ($code == 200) {
					//tmhUtilities::pr(json_decode($this->twitter->response['response']));
				} else {
					//tmhUtilities::pr($this->twitter->response['response']);
				}


			}

			if(!$this->enablePosting) {
				return false;
			}
			return true;
		}



	//-----------------------------------------------------------------------------------------------------------------------------------
	//-- FACEBOOK CUSTOM ----------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------

		public function fb_getUserAccessToken() {

			//this sucker lasts for a while but it does change.
			//there's a cron job that runs nightly that saves the newest user token into the settings db table

			$u = $this->facebook->getLoginUrl(array('scope'=>'manage_pages,publish_stream,offline_access'));

			if(!isset($_GET['code'])) {
				header('Location:'.$u);
				exit;
			}

			$access_token = $this->facebook->getAccessToken();

			return $access_token;

		}

		private function fb_getAccounts() {
			//PULL DATA
			$readfeed = $this->facebook->api('/me/accounts','GET', array(
								'access_token' => $this->user_access_token
						)	);

			foreach($readfeed['data'] as $r) {
				if($r['id'] == $this->facebook_page_id) {
					$this->page_access_token = $r['access_token'];
				}
			}
		}


}